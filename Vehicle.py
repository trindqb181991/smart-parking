__author__ = 'TriNguyenDang'
from Location import *
from Prefer import *
from Slot import *

CONST_A = 'Ascending'
CONST_D = 'Descending'

class Vehicle:
    ID = None
    Destination = None
    ParkingTime = None
    ListPrefer = None
    Matched = None
    Cost = None
    def __init__(self,Lists):
        self.ID = Lists[0]
        self.Destination = Lists[1]
        self.ParkingTime = Lists[2]
        self.ListPrefer = Lists[3]
        self.Matched = Lists[4]
        self.Cost = Lists[5]
    @staticmethod
    def InitVehicle():
        X = 1
        Y = 1
        while(X>=1)and(X<=CONST_COLUMN)and(Y>=1)and(Y<=CONST_ROW) or (X <= -1)and(X >= -CONST_COLUMN)and(Y>=1)and(Y<=CONST_ROW) or (X>=1)and(X<=CONST_COLUMN)and(Y<= -1)and(Y >= -CONST_ROW) or (X <= -1)and(X >= -CONST_COLUMN)and(Y<= -1)and(Y >=-CONST_ROW):
            X = random.randint(1,70)*random.choice([-1,1])
            Y = random.randint(1,70)*random.choice([-1,1])
        ID = Location( X,Y )
        XYD = random.choice([0,1,2,3])
        #print XYD
        Des = Location(DES[XYD][0],DES[XYD][1])
        T = round(random.uniform(0.13,2.0),2)
        LP = ManagerPrefer()
        #LP.add(Prefer(Location(0,0),1.2))
        M = Location(0,0)
        Cost = 0
        return ID,Des,T,LP,M,Cost
    def __str__(self):
        return "ID: %s,Des: %s,Time: %s,Pre: %s,M:%s,C:%s"%(self.ID,self.Destination,self.ParkingTime,self.ListPrefer,self.Matched,self.Cost)

    def __repr__(self):
        return "\n<ID: %s,Des: %s,Time: %s,Pre: %s,M:%s,C:%s>\n"%(self.ID,self.Destination,self.ParkingTime,self.ListPrefer,self.Matched,self.Cost)

    def __getitem__(self, item):
        if(item == 'ID'):
            return self.ID
        elif(item == 'Destination'):
            return self.Destination
        elif(item == 'ParkingTime'):
            return self.ParkingTime
        elif(item == 'Matched'):
            return self.Matched
        elif(item == 'Cost'):
            return self.Cost
        elif(item == 'ListPrefer'):
            return self.ListPrefer
    def __setitem__(self, key, value):
        if(key == 'ID'):
            self.ID = value
        elif(key == 'Destiantion'):
            self.Destination = value
        elif(key == 'ParkingTime'):
            self.ParkingTime = value
        elif(key == 'ListPrefer'):
            self.ListPrefer = value
        elif(key == 'Matched'):
            self.Matched = value
        elif(key == 'Cost'):
            self.Cost = value

    def CalculatePrefer(self,ListSlot,Destination,Type):

        tmp = ManagerPrefer()
        for subSlot in ListSlot:
            tmpLocation = subSlot['ID']
            tmpCost = subSlot['Fee']*self['ParkingTime'] + CONST_COST_DRIVING * (self['ID'] - subSlot['ID'])/10 + CONST_COST_WALKING*(Destination - subSlot['ID'])/100
            tmp.add(Prefer(tmpLocation,tmpCost))
        if(Type == 'Ascending'):
            ManagerPrefer.QuickSort(tmp.ListPrefer)
        elif(Type == 'Descending'):
            ManagerPrefer.QuickSort2(tmp.ListPrefer)
        self['ListPrefer'] = tmp

    def DrawMatched(self,Color,Marker):
        plt.plot([self['ID']['X'],self['Matched']['X']],[self['ID']['Y'],self['Matched']['Y']],color = Color,marker = Marker)




    pass

class ManagerVehicle(object):

    typeA = None
    typeB = None
    typeC =None
    typeD = None
    numVehicle = None
    def __init__(self,numVehicle):
        self.numVehicle = numVehicle
        self.typeB = []
        self.typeA = []
        self.typeC = []
        self.typeD = []
        i = 1
        while(i <= self.numVehicle):
            tmp = Vehicle(Vehicle.InitVehicle())
            if(tmp['Destination'] == DA):
                self.typeA.append(tmp)
            elif(tmp['Destination'] == DB):
                self.typeB.append(tmp)
            elif(tmp['Destination'] == DC):
                self.typeC.append(tmp)
            elif(tmp['Destination'] == DD):
                self.typeD.append(tmp)
            i+=1


    def __getitem__(self, item):
        if(item == 'numVehicle'):
            return self.numVehicle
        elif(item == 'A'):
            return self.typeA
        elif(item == 'B'):
            return self.typeB
        elif(item == 'C'):
            return self.typeC
        elif(item == 'D'):
            return self.typeD

    def Acceptation(self,ListSlot):
        for subV in self['A']:
            subV.CalculatePrefer(ListSlot['A'],DA,CONST_A)
        for subV in self['B']:
            subV.CalculatePrefer(ListSlot['B'],DB,CONST_A)
        for subV in self['C']:
            subV.CalculatePrefer(ListSlot['C'],DC,CONST_A)
        for subV in self['D']:
            subV.CalculatePrefer(ListSlot['D'],DD,CONST_A)
        i = 0
        while(i<len(self['A'])):
            tmpAccept = self['A'][i]['ListPrefer'][0]
            self['A'][i]['Matched'] = tmpAccept['ID']
            self['A'][i]['Cost'] = tmpAccept['Cost']
            self['A'][i]['ListPrefer'] = ManagerPrefer()
            for j in range(i+1,len(self['A'])):
                self['A'][j]['ListPrefer'].Delete(tmpAccept['ID'])
            i+=1

        i = 0
        while(i<len(self['B'])):
            tmpAccept = self['B'][i]['ListPrefer'][0]
            self['B'][i]['Matched'] = tmpAccept['ID']
            self['B'][i]['Cost'] = tmpAccept['Cost']
            self['B'][i]['ListPrefer'] = ManagerPrefer()
            for j in range(i+1,len(self['B'])):
                self['B'][j]['ListPrefer'].Delete(tmpAccept['ID'])
            i+=1

        i = 0
        while(i<len(self['C'])):
            tmpAccept = self['C'][i]['ListPrefer'][0]
            self['C'][i]['Matched'] = tmpAccept['ID']
            self['C'][i]['Cost'] = tmpAccept['Cost']
            self['C'][i]['ListPrefer'] = ManagerPrefer()
            for j in range(i+1,len(self['C'])):
                self['C'][j]['ListPrefer'].Delete(tmpAccept['ID'])
            i+=1
        i = 0
        while(i<len(self['D'])):
            tmpAccept = self['D'][i]['ListPrefer'][0]
            self['D'][i]['Matched'] = tmpAccept['ID']
            self['D'][i]['Cost'] = tmpAccept['Cost']
            self['D'][i]['ListPrefer'] = ManagerPrefer()
            for j in range(i+1,len(self['D'])):
                self['D'][j]['ListPrefer'].Delete(tmpAccept['ID'])
            i+=1

    def PrintMatched(self):
        for subV in self['A']:
            print subV['ID'],'<-->',subV['Matched']
        for subV in self['B']:
            print subV['ID'],'<-->',subV['Matched']
        for subV in self['C']:
            print subV['ID'],'<-->',subV['Matched']
        for subV in self['D']:
            print subV['ID'],'<-->',subV['Matched']
    def DrawMatched(self):
        for subV in self['A']:
            subV.DrawMatched('r','*')
        for subV in self['B']:
            subV.DrawMatched('b','o')
        for subV in self['C']:
            subV.DrawMatched('g','^')
        for subV in self['D']:
            subV.DrawMatched('k','v')
        plt.show()


    def __str__(self):
        return "%s\n%s\n%s\n%s\n%s"%(self.numVehicle,self.typeA,self.typeB,self.typeC,self.typeD)

    def __repr__(self):
        return "<%s\n%s\n%s\n%s\n%s>"%(self.numVehicle,self.typeA,self.typeB,self.typeC,self.typeD)


    def getIDs(self):
        Ids =[]
        for sub in self['A']:
            Ids.append(sub['ID'])
        for sub in self['B']:
            Ids.append(sub['ID'])
        for sub in self['C']:
            Ids.append(sub['ID'])
        for sub in self['D']:
            Ids.append(sub['ID'])
        return Ids
    pass


MV = ManagerVehicle(100)
MS = ManagerSlot(ManagerSlot.InitListSlots())
MV.Acceptation(MS)
MV.PrintMatched()
MS.DrawSlot()
MV.DrawMatched()





'''
I1 = MV.getIDs()
I2 = MS.getIDs()
for x in I1:
    plt.plot(x['X'],x['Y'],'ro')
for x in I2:
    plt.plot(x['X'],x['Y'],'b*')
plt.plot(DA['X'],DA['Y'],'r^')
plt.plot(DB['X'],DB['Y'],'b^')
plt.plot(DC['X'],DC['Y'],'g^')
plt.plot(DD['X'],DD['Y'],'y^')
plt.show()
'''