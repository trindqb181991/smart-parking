__author__ = 'TriNguyenDang'
from Location import *
import numpy as np
import matplotlib.pyplot as plt
from Prefer import *

CONST_COLUMN = 10
CONST_ROW = 10

class Slot(object):
    ID = None
    Fee = None
    State = None
    ListPrefer = None
    Matched = None
    Cost = None
    def __init__(self,List):
        self.ID = List[0]
        self.Fee = List[1]
        self.State = List[2]
        self.ListPrefer = List[3]
        self.Matched = List[4]
        self.Cost = List[5]
    def __getitem__(self, item):
        if(item == 'ID'):
            return self.ID
        elif(item == 'Fee'):
            return self.Fee
        elif(item == 'State'):
            return self.State
        elif(item == 'ListPrefer'):
            return self.ListPrefer
        elif(item == 'Matched'):
            return self.Matched
        elif(item == 'Cost'):
            return self.Cost
    def __setitem__(self, key, value):
        if(key == 'ID'):
            self.ID = value
        elif(key == 'Fee'):
            self.Fee = value
        elif(key == 'State'):
            self.State = value
        elif(key == 'ListPrefer'):
            self.ListPrefer = value
    def CalculatePrefer(self,ListVehicle):
        tmp = ManagerPrefer()
        for subVehicle in ListVehicle:
            tmpLocation = subVehicle['ID']
            tmpCost = self['Fee']*subVehicle['ParkingTime']
            tmp.add(Prefer(tmpLocation,tmpCost))
        ManagerPrefer.QuickSort2(tmp.ListPrefer)
        self['ListPrefer'] = tmp

    def __str__(self):
        return "ID: %s,Fee: %s,State: %s,LP:%s"%(self.ID,self.Fee,self.State,self.ListPrefer)
    def __repr__(self):
        return "<ID: %s,Fee: %s,State: %s,LP:%s>\n"%(self.ID,self.Fee,self.State,self.ListPrefer)

    pass
class ManagerSlot(object):
    SlotList = None
    NearBy = None
    Quota = None
    Name  = None
    def __init__(self,Data):
        self.SlotList = Data[0]
        self.NearBy = Data[1]
        self.Quota = Data[2]
    @staticmethod
    def InitData(Quota,Name):
        N = int(math.sqrt(Quota))
        for i in range(1,N+1):
            for j in range(1,N+1):
                print '1'

class ManagerSlot:
    typeA = None
    NearbyA = [ DA,DB,DD,DC]
    typeB = None
    NearbyB = [DB,DA,DC,DD]
    typeC = None
    NearbyC = [ DC, DB,DD,DA]
    typeD = None
    NearbyD = [DD,DC,DA,DB]
    def __init__(self,List):
        self.typeA = List[0]
        self.typeB = List[1]
        self.typeC = List[2]
        self.typeD = List[3]

    def __getitem__(self, item):
        if(item == 'A'):
            return self.typeA
        elif(item == 'B'):
            return self.typeB
        elif(item == 'C'):
            return self.typeC
        elif(item == 'D'):
            return self.typeD
        elif(item == 'LA'):
            return self.NearbyA
        elif(item == 'LB'):
            return self.NearbyB
        elif(item == 'LC'):
            return self.NearbyC
        elif(item == 'LD'):
            return self.NearbyD
    def getState(self,type,ID):
        if(type == 'A'):
            for x in self.typeA:
                if(ID == x['ID']):
                    return x['State']
        elif(type == 'B'):
            for x in self.typeB:
                if(ID == x['ID']):
                    return x['State']
        if(type == 'C'):
            for x in self.typeC:
                if(ID == x['ID']):
                    return x['State']
        if(type == 'D'):
            for x in self.typeD:
                if(ID == x['ID']):
                    return x['State']
    @staticmethod
    def InitListSlots():
        tmpA = []
        tmpB = []
        tmpC = []
        tmpD = []
        for X in range(1,CONST_ROW):
            for Y in range(1,CONST_COLUMN):
                tmpA.append(Slot([Location(X,Y),random.uniform(1.7,2.9),True,ManagerPrefer(),Location(0,0),0]))
                tmpB.append(Slot([Location(-X,Y),random.uniform(1.7,2.9),True,ManagerPrefer(),Location(0,0),0 ]))
                tmpC.append(Slot([Location(X,-Y),random.uniform(1.7,2.9),True,ManagerPrefer() ,Location(0,0),0]))
                tmpD.append(Slot([Location(-X,-Y),random.uniform(1.7,2.9),True,ManagerPrefer() ,Location(0,0),0]))
        return tmpA,tmpB,tmpC,tmpD

    def __str__(self):
        return  "%s,%s,%s,%s,%s\n,%s\n,%s\n,%s\n"%(self.typeA,self.typeB,self.typeC,self.typeD,self.NearbyA,self.NearbyB,self.NearbyC,self.NearbyD)
    def __repr__(self):
        return  "[\n%s,%s,%s,%s,%s\n,%s\n,%s\n,%s\n]"%(self.typeA,self.typeB,self.typeC,self.typeD,self.NearbyA,self.NearbyB,self.NearbyC,self.NearbyD)


    def DrawSlot(self):
        #plt.axis([-100,100,-100,100])
        plt.grid(True)
        for subA in self['A']:
            plt.plot(subA['ID']['X'],subA['ID']['Y'],'ro')
        for subA in self['B']:
            plt.plot(subA['ID']['X'],subA['ID']['Y'],'bx')
        for subA in self['C']:
            plt.plot(subA['ID']['X'],subA['ID']['Y'],'g*')
        for subA in self['D']:
            plt.plot(subA['ID']['X'],subA['ID']['Y'],'yo')



    def getIDs(self):
        Ids = []
        for sub in self['A']:
            Ids.append(sub['ID'])
        for sub in self['B']:
            Ids.append(sub['ID'])
        for sub in self['C']:
            Ids.append(sub['ID'])
        for sub in self['D']:
            Ids.append(sub['ID'])
        return Ids
    pass



