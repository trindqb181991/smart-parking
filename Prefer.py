__author__ = 'TriNguyenDang'
#from Location import *
class Prefer(object):
    ID = None
    Cost = None
    def __init__(self,ID,Cost):
        self.ID = ID
        self.Cost = Cost
    def __getitem__(self, item):
        if(item == 'ID'):
            return self.ID
        elif(item == 'Cost'):
            return self.Cost
    def __setitem__(self, key, value):
        if(key == 'ID'):
            self.ID = value
        elif(key == 'Cost'):
            self.Cost =value
    def __lt__(self, other):
        return self.Cost < other.Cost

    def __gt__(self, other):
        return self.Cost > other.Cost

    def __eq__(self, other):
        return self.ID == other.ID

    def __str__(self):
        return "%s,%s"%(self.ID,self.Cost)
    def __repr__(self):
        return "<%s,%s>"%(self.ID,self.Cost)

    def __le__(self, other):
        return self.Cost <= other.Cost

    def __ge__(self, other):
        return self.Cost >= other.Cost


    pass

class ManagerPrefer(object):
    ListPrefer = None

    def __init__(self):
        self.ListPrefer = []

    def add(self,value):
        self.ListPrefer.append(value)

    def __getitem__(self, i):
        return self.ListPrefer[i]

    def Delete(self,ID):
        for sub in self.ListPrefer:
            if(sub['ID']==ID):
                self.ListPrefer.remove(sub)


    def __str__(self):
        return "%s"%(self.ListPrefer)
    def __repr__(self):
        return "<%s>"%(self.ListPrefer)




    @staticmethod
    def QuickSort(List):
        ManagerPrefer.QuickSortPartition(List,0,len(List)-1)

    @staticmethod
    def Partition(List,First,Last):
        Pivot = List[First] #Key
        Left = First + 1
        Right = Last
        Done = False
        while not(Done):
            while(Left <= Right)and(List[Left] <= Pivot):
                Left+=1
            while (List[Right] >= Pivot)and(Right>=Left):
                Right-=1
            if(Right<Left):
                Done =True
            else:
                Ltmp = List[Left]
                List[Left] = List[Right]
                List[Right] = Ltmp

        Ftmp = List[First]
        List[First] = List[Right]
        List[Right] = Ftmp
        return Right
    @staticmethod
    def QuickSortPartition(List,First,Last):
        if(First<Last):
            MidlePoint = ManagerPrefer.Partition(List,First,Last)
            ManagerPrefer.QuickSortPartition(List,First,MidlePoint-1)
            ManagerPrefer.QuickSortPartition(List,MidlePoint+1,Last)

    @staticmethod
    def QuickSort2(List):
        ManagerPrefer.QuickSortPartition2(List,0,len(List)-1)

    @staticmethod
    def Partition2(List,First,Last):
        Pivot = List[First] #Key
        Left = First + 1
        Right = Last
        Done = False
        while not(Done):
            while(Left <= Right)and(List[Left] >= Pivot):
                Left+=1
            while (List[Right] <= Pivot)and(Right >= Left):
                Right-=1
            if(Right<Left):
                Done =True
            else:
                Ltmp = List[Left]
                List[Left] = List[Right]
                List[Right] = Ltmp

        Ftmp = List[First]
        List[First] = List[Right]
        List[Right] = Ftmp
        return Right
    @staticmethod
    def QuickSortPartition2(List,First,Last):
        if(First<Last):
            MidlePoint = ManagerPrefer.Partition2(List,First,Last)
            ManagerPrefer.QuickSortPartition2(List,First,MidlePoint-1)
            ManagerPrefer.QuickSortPartition2(List,MidlePoint+1,Last)

