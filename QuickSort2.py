__author__ = 'TriNguyenDang'
def QuickSort2(List):
    QuickSortPartition2(List,0,len(List)-1)


def Partition2(List,First,Last):
    Pivot = List[First][0] #Key
    Left = First + 1
    Right = Last
    Done = False
    while not(Done):
        while(Left <= Right)and(List[Left][0] >= Pivot):
            Left+=1
        while (List[Right][0] <= Pivot)and(Right>=Left):
            Right-=1
        if(Right<Left):
            Done =True
        else:
            Ltmp = List[Left]
            List[Left] = List[Right]
            List[Right] = Ltmp
    Ftmp = List[First]
    List[First] = List[Right]
    List[Right] = Ftmp
    return Right
def QuickSortPartition2(List,First,Last):
    if(First<Last):
        MidlePoint = Partition2(List,First,Last)
        QuickSortPartition2(List,First,MidlePoint-1)
        QuickSortPartition2(List,MidlePoint+1,Last)