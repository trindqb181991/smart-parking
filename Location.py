__author__ = 'TriNguyenDang'
import math
import random
from Sorting import *
from QuickSort2 import *
import numpy as np
import matplotlib.pyplot as plt

DES = [[-60,-60],[60,-60],[60,60],[-60,60]]
CONST_COST_DRIVING=0.31
CONST_COST_WALKING=0.1



class Location(object):
    X = 0
    Y = 0
    def __init__(self,X,Y):
        self.X = X
        self.Y = Y
    def __getitem__(self, item):
        if(item == 'X'):
            return self.X
        elif(item =='Y'):
            return self.Y
    def __setitem__(self, key ,value):
        if(key == 'X'):
            self.X = value
        if(key == 'Y'):
            self.Y = value
    def __str__(self):
        return "(%s,%s)"%(self.X,self.Y)
    def __repr__(self):
        return "<%s,%s>"%(self.X,self.Y)
    def __sub__(self, other):
        return math.sqrt(pow(other['X'] - self['X'],2) + pow(other['Y'] - self['Y'],2))

    def __eq__(self, other):
        return (self['X'] == other['X'])and(self['Y']==other['Y'])
    pass
#---------------Given for location of 4 destination------------------#
DA = Location(DES[0][0],DES[0][1])
DB = Location(DES[1][0],DES[1][1])
DC = Location(DES[2][0],DES[2][1])
DD = Location(DES[3][0],DES[3][1])
DN = [DA,DB,DC,DD]

#---------------------------------------------------------------------#











'''

def PartitionVehicle(V):
    typeVA = []
    typeVB = []
    typeVC = []
    typeVD = []
    #print VA,VB,VC,VD
    for subV in V['Vehicles']:
        #print subV
        if(subV['Destination'] == VA):
            typeVA.append(subV)
        elif(subV['Destination'] == VB):
            typeVB.append(subV)
        elif(subV['Destination'] == VC):
            typeVC.append(subV)
        elif(subV['Destination'] == VD):
            typeVD.append(subV)
    return typeVA,typeVB,typeVC,typeVD
Va,Vb,Vc,Vd = PartitionVehicle(V)
#print Va,Vb,Vc,Vd


def VPrefer(Va,Vb,Vc,Vd):
    Pa = P['A']

    Pb = P['B']

    Pc = P['C']

    Pd = P['D']

    tmpVa = []
    tmpVb = []
    tmpVc = []
    tmpVd = []
    for subVa in Va:
        for subPa in Pa:
            Cost = CONST_COST_DRIVING * (subVa['ID'] - subPa['ID'])/10 + CONST_COST_WALKING * ((subPa['ID'] - VA)/10) + \
                   subVa['ParkingTime']*subPa['Fee']
            tmpVa.append([Cost,subPa['ID']])
        QuickSort(tmpVa)
        subVa['ListPrefer'] = tmpVa
        tmpVa = []

    for subVb in Vb:
        for subPb in Pb:
            Cost = CONST_COST_DRIVING * (subVb['ID'] - subPb['ID'])/10 + CONST_COST_WALKING * ((subPb['ID'] - VB)/10) +\
                   subVb['ParkingTime']*subPb['Fee']
            tmpVb.append([Cost,subPb['ID']])
        QuickSort(tmpVb)
        subVb['ListPrefer'] = tmpVb
        tmpVb =[]
    for subVc in Vc:
        for subPc in Pc:
            Cost = CONST_COST_DRIVING * (subVc['ID'] - subPc['ID'])/10 + CONST_COST_WALKING * ((subPc['ID'] - VC)/10) +\
                   subVc['ParkingTime']*subPc['Fee']
            tmpVc.append([Cost,subPc['ID']])
        QuickSort(tmpVc)
        subVc['ListPrefer'] = tmpVc
        tmpVc = []
    for subVd in Vd:
        for subPd in Pd:
            Cost = CONST_COST_DRIVING * (subPd['ID'] - subPd['ID'])/10 + CONST_COST_WALKING * ((subPd['ID'] - VD)/10) + \
                   subVd['ParkingTime']*subPd['Fee']
            tmpVd.append([Cost,subPd['ID']])
        QuickSort(tmpVd)
        subVd['ListPrefer'] = tmpVd
        tmpVd = []
    return Va,Vb,Vc,Vd

def SPrefer(Va,Vb,Vc,Vd):
    Pa = P['A']

    Pb = P['B']

    Pc = P['C']

    Pd = P['D']

    tmpVa = []
    tmpVb = []
    tmpVc = []
    tmpVd = []
    for subPa in Pa:
        for subVa in Va:
            Cost = CONST_COST_DRIVING * (subVa['ID'] - subPa['ID'])/10 + CONST_COST_WALKING * ((subPa['ID'] - VA)/10) + \
                   subVa['ParkingTime']*subPa['Fee']
            tmpVa.append([Cost,subVa['ID']])
        QuickSort2(tmpVa)
        subPa['ListPrefer'] = tmpVa
        tmpVa = []

    for subPb in Pb:
        for subVb in Vb:
            Cost = CONST_COST_DRIVING * (subVb['ID'] - subPb['ID'])/10 + CONST_COST_WALKING * ((subPb['ID'] - VB)/10) +\
                   subVb['ParkingTime']*subPb['Fee']
            tmpVb.append([Cost,subVb['ID']])
        QuickSort2(tmpVb)
        subPb['ListPrefer'] = tmpVb
        tmpVb =[]
    for subPc in Pc:
        for subVc in Vc:
            Cost = CONST_COST_DRIVING * (subVc['ID'] - subPc['ID'])/10 + CONST_COST_WALKING * ((subPc['ID'] - VC)/10)+\
                   subVc['ParkingTime']*subPc['Fee']
            tmpVc.append([Cost,subVc['ID']])
        QuickSort2(tmpVc)
        subPc['ListPrefer'] = tmpVc
        tmpVc = []
    for subPd in Pd:
        for subVd in Vd:
            Cost = CONST_COST_DRIVING * (subPd['ID'] - subPd['ID'])/10 + CONST_COST_WALKING * ((subPd['ID'] - VD)/10) + \
                   subVd['ParkingTime']*subPd['Fee']
            tmpVd.append([Cost,subVd['ID']])
        QuickSort2(tmpVd)
        subPd['ListPrefer'] = tmpVd
        tmpVd = []
    return Pa,Pb,Pc,Pd

def VAcception():
    V1,V2,V3,V4 = VPrefer(Va,Vb,Vc,Vd)

    #print len(V1),len(V2),len(V3),len(V4)
    for i in range(0,len(V1)):
        tmpLocation = V1[i]['ListPrefer'][0][1]
        while len(V1[i]['ListPrefer']) >1:
            V1[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V1)):
            for k in range(0,len(V1[j]['ListPrefer'])-1):
                if(V1[j]['ListPrefer'][k][1] == tmpLocation):
                    V1[j]['ListPrefer'].pop(k)
    #print V1
    for i in range(0,len(V2)):
        tmpLocation = V2[i]['ListPrefer'][0][1]
        while len(V2[i]['ListPrefer']) >1:
            V2[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V2)):
            for k in range(0,len(V2[j]['ListPrefer'])-1):
                if(V2[j]['ListPrefer'][k][1] == tmpLocation):
                    V2[j]['ListPrefer'].pop(k)
    #print V2
    for i in range(0,len(V3)):
        tmpLocation = V3[i]['ListPrefer'][0][1]
        while len(V3[i]['ListPrefer']) >1:
            V3[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V3)):
            for k in range(0,len(V3[j]['ListPrefer'])-1):
                if(V3[j]['ListPrefer'][k][1] == tmpLocation):
                    V3[j]['ListPrefer'].pop(k)
    #print V3
    for i in range(0,len(V4)):
        tmpLocation = V4[i]['ListPrefer'][0][1]
        while len(V4[i]['ListPrefer']) >1:
            V4[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V4)):
            for k in range(0,len(V4[j]['ListPrefer'])-1):
                if(V4[j]['ListPrefer'][k][1] == tmpLocation):
                    V4[j]['ListPrefer'].pop(k)
    #print V4
    return V1,V2,V3,V4
def SAcception():
    S1,S2,S3,S4 = SPrefer(Va,Vb,Vc,Vd)
    for i in range(0,len(Va)):
        tmpLocation = S1[i]['ListPrefer'][0][1]
        while len(S1[i]['ListPrefer']) >1:
            S1[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(S1)):
            for k in range(0,len(S1[j]['ListPrefer'])-1):
                if(S1[j]['ListPrefer'][k][1] == tmpLocation):
                    S1[j]['ListPrefer'].pop(k)
    for i in range(len(Va),len(S1)):
        while len(S1[i]['ListPrefer']) >0:
            S1[i]['ListPrefer'].pop(0)
    #print S1
    for i in range(0,len(Vb)):
        tmpLocation = S2[i]['ListPrefer'][0][1]
        while len(S2[i]['ListPrefer']) >1:
            S2[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(S2)):
            for k in range(0,len(S2[j]['ListPrefer'])-1):
                if(S2[j]['ListPrefer'][k][1] == tmpLocation):
                    S2[j]['ListPrefer'].pop(k)
    for i in range(len(Vb),len(S2)):
        while len(S2[i]['ListPrefer']) >0:
            S2[i]['ListPrefer'].pop(0)
    #print S2
    for i in range(0,len(Vc)):
        tmpLocation = S3[i]['ListPrefer'][0][1]
        while len(S3[i]['ListPrefer']) >1:
            S3[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(S3)-1):
            for k in range(0,len(S3[j]['ListPrefer'])-1):
                if(S3[j]['ListPrefer'][k][1] == tmpLocation):
                    S3[j]['ListPrefer'].pop(k)
    for i in range(len(Vc),len(S3)):
        while len(S3[i]['ListPrefer']) >0:
            S3[i]['ListPrefer'].pop(0)
    #print S3
    for i in range(0,len(Vd)):
        tmpLocation = S4[i]['ListPrefer'][0][1]
        while len(S4[i]['ListPrefer']) >1:
            S4[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(S4)-1):
            for k in range(0,len(S4[j]['ListPrefer'])-1):
                if(S4[j]['ListPrefer'][k][1] == tmpLocation):
                    S4[j]['ListPrefer'].pop(k)
    for i in range(len(Vd),len(S4)):
        while len(S4[i]['ListPrefer']) >0:
            S4[i]['ListPrefer'].pop(0)
    #print S4
    return S1,S2,S3,S4

def Matching():
    V1,V2,V3,V4 = VPrefer(Va,Vb,Vc,Vd)
    S1,S2,S3,S4 = SPrefer(Va,Vb,Vc,Vd)

    for i in range(0,len(V1)):
        tmpLocation = V1[i]['ListPrefer'][0][1]
        for subS in S1:
            if(subS['ListPrefer'][0][1] == tmpLocation):
                while len(V1[i]['ListPrefer']) >1:
                    V1[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V1)):
            for k in range(0,len(V1[j]['ListPrefer'])-1):
                if(V1[j]['ListPrefer'][k][1] == tmpLocation):
                    V1[j]['ListPrefer'].pop(k)
    #print V1
    for i in range(0,len(V2)):
        tmpLocation = V2[i]['ListPrefer'][0][1]
        for subS in S2:
            if(subS['ListPrefer'][0][1] == tmpLocation):
                while len(V2[i]['ListPrefer']) >1:
                    V2[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V2)):
            for k in range(0,len(V2[j]['ListPrefer'])-1):
                if(V2[j]['ListPrefer'][k][1] == tmpLocation):
                    V2[j]['ListPrefer'].pop(k)
    #print V2
    for i in range(0,len(V3)):
        tmpLocation = V3[i]['ListPrefer'][0][1]
        for subS in S3:
            if(subS['ListPrefer'][0][1] == tmpLocation):
                while len(V3[i]['ListPrefer']) >1:
                    V3[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V3)):
            for k in range(0,len(V3[j]['ListPrefer'])-1):
                if(V3[j]['ListPrefer'][k][1] == tmpLocation):
                    V3[j]['ListPrefer'].pop(k)
    #print V3
    for i in range(0,len(V4)):
        tmpLocation = V4[i]['ListPrefer'][0][1]
        for subS in S4:
            if(subS['ListPrefer'][0][1] == tmpLocation):
                while len(V4[i]['ListPrefer']) >1:
                    V4[i]['ListPrefer'].pop(1)

        for j in range(i+1,len(V4)):
            for k in range(0,len(V4[j]['ListPrefer'])-1):
                if(V4[j]['ListPrefer'][k][1] == tmpLocation):
                    V4[j]['ListPrefer'].pop(k)
    #print V4
    return V1,V2,V3,V4



SA = []
SB = []
SC = []
SD = []
V1,V2,V3,V4 = VAcception()
S1,S2,S3,S4 = SAcception()
#print S1,S2,S3,S4

print V1
print V2
print V3
print V4

tmpRs = []
for subv in V1:
    tmpRs.append(subv['ListPrefer'][0][0])
for subv in V2:
    tmpRs.append(subv['ListPrefer'][0][0])
for subv in V3:
    tmpRs.append(subv['ListPrefer'][0][0])
for subv in V4:
    tmpRs.append(subv['ListPrefer'][0][0])


tmpSs = []
for subs in S1:
    if(subs['ListPrefer'] != []):
        tmpSs.append(subs['ListPrefer'][0][0])
for subs in S2:
    if(subs['ListPrefer'] != []):
        tmpSs.append(subs['ListPrefer'][0][0])
for subs in S3:
    if(subs['ListPrefer'] !=[]):
        tmpSs.append(subs['ListPrefer'][0][0])
for subs in S4:
    if(subs['ListPrefer'] != []):
        tmpSs.append(subs['ListPrefer'][0][0])
tmpSs = sorted(tmpSs)

for i in range(1,49):
    for j in range(1,49):
        SA.append([i,j])
        SB.append([-i,j])
        SC.append([i,-j])
        SD.append([-i,-j])
for (x,y) in SA:
    plt.plot(x,y,'bo')
for (x,y) in SB:
    plt.plot(x,y,'ro')
for (x,y) in SC:
    plt.plot(x,y,'yo')
for (x,y) in SD:
    plt.plot(x,y,'go')
for subV in V1:
    plt.plot(subV['ID']['X'],subV['ID']['Y'],'bo')
for subV in V2:
    plt.plot(subV['ID']['X'],subV['ID']['Y'],'ro')
for subV in V3:
    plt.plot(subV['ID']['X'],subV['ID']['Y'],'yo')
for subV in V4:
    plt.plot(subV['ID']['X'],subV['ID']['Y'],'go')


print len(tmpRs),len(tmpSs)
tmpRs = sorted(tmpRs)
x = np.arange(0,len(tmpRs))
y = tmpRs
xx = np.arange(0,len(tmpSs))
yy = tmpSs
plt.plot(x,y,'r*-',label='SO')
plt.plot(xx,yy,'bo-',label='VO')
plt.title('Parking')
plt.xlabel('numUE')
plt.ylabel('Value')
plt.legend(loc=0)
#plt.axis([-500,500,-500,500])
plt.grid(True)
plt.show()



#SAcception()

VC = ManagerVehicle(10)
print VC



A = Location(0,0)
print A
B = Location(2,3)
A = B
print A
C1 = Vehicle(Vehicle.InitVehicle())
print C1
C2 = Vehicle(Vehicle.InitVehicle())
print C2
print C2['ID'] - C1['ID']
C2['ID'] = C1['ID']
print C2
K = ManagerSlot(ManagerSlot.InitListSlots())
print K['A'][0]['ID']
'''

